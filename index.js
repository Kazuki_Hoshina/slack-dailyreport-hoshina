require('util').inspect.defaultOptions.depth = 6

const Slack = require('slack')
const bot = new Slack({token: 'xoxp-2334328671-734467491584-754786873094-93aae9b1c169552727ba821e27be86ef'})
// bot.chat.postMessage({channel: '#prj-up-slack-dailyreport', text:'hello'}).then(console.log).catch(console.error)

const smb = require('slack-message-builder')
const json1 = smb().text('I am a test message http://slack.com')
  .attachment()
    .text("And here's an attachment!")
  .end()
.json()

const json2 = smb()
  .text('Pick a user(text)')
  .attachment()
    .text('Pick a user(attachment/text)')
    .fallback('Pick a user(attachment/fallback)')
    .callbackId('user_callback')
    .select()
      .name('option_group')
      .text('Static Option Groups')
      .optionGroup()
        .text('Option header')
        .option('some text', 'a value')
        .option('some more text', 'moar value')
      .end()
      .optionGroup()
        .text('Second Option header')
        .option('some text', 'a value')
        .option('some more text', 'moar value')
      .end()
    .end()
  .end()
.json();


async function do_slack() {
    console.log('data', json1);
    
    const result1 = await bot.chat.postMessage({
        channel: '#prj-up-slack-dailyreport',
        text: json1.text,
        attachments: json1.attachments
    })
    console.log('result', result1);
    

    console.log('data', json2);
    //spread演算子
    const result2 = await bot.chat.postMessage({
        channel: '#prj-up-slack-dailyreport',
        ...json2
    })
    console.log('result', result2);
    
}

do_slack().catch(console.error);